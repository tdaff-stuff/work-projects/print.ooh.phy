# OOH Thermal Printer software

The Out of Hours emergency printer system consists of a thermal printer
(think till-roll) attached to a Raspberry Pi running software that monitors
the building occupancy using the sign-in data from an
[Out of Hours server](https://codeshare.phy.cam.ac.uk/itservices.phy/ooh.phy).

This repository holds instructions for building an emergency printer and
the software that can be installed for monitoring.

## Monitoring software

This software, intended to be run on a Raspberry Pi with a thermal printer
like the [pipsta](http://www.pipsta.co.uk/), monitors the GPIO on the Pi
and will print the list of "currently signed in" people from the configured
server when contact is made between pin 5 and ground (probably with a push
button connected between the pins). A cached list of occupants is maintained
in case the network goes down.

## Assembly instructions

![assembled printer](assembly/printer.jpg "Fully assembled printer")

See [assembly instructions](assembly/README.md).

## Installation

Installation using Ansible is recommended as all the information for all
the systems can be kept in an inventory file and the systems are easily
updated by re-running the playbook.

### Ansible

Ansible can be used to deploy and maintain a server with minial configuration
required. Instructions are given for setting up SSH on
[Raspbian](https://www.raspberrypi.org/downloads/raspbian/), but other
distributions may work (untested).

* Install Rasbian on the microSD card using the [instructions for installing
  an image](https://www.raspberrypi.org/documentation/installation/installing-images/README.md) (not NOOBS).
* With the microSD card still in your computer, add a file `/boot/ssh` on the SD
  card **boot** partition. This will enable the SSH server so that the system
  can be run completely headless.
* Add any SSH keys that will be used to manage the system to
  `/root/.ssh/authorized_keys` on the SD card **root** partition so that the
  system can be logged in to without a password when deploying with ansible.
* Register your Pi in your DHCP server, and add the IP as a "kiosk" to the
  OOH server so that it can access the list of people.
* Insert the microSD card and boot the Pi.
* Create an inventory, or add the system to an existing inventory:
```yaml
default:
  hosts:
    123.45.67.89:
  vars:
    ooh_print_url: https://your.print.server/print
```
* Run the Ansible playbook: `ansible-playbook printer-play.yml -i your-inventory.yml`
* If you have installed one of the buster releases you may need to manually
  run `apt-get update --allow-releaseinfo-change` on the device.

#### Ansible variables

* `ooh_print_url`: The URL where the list of people can be accessed.
* `ooh_env_dir`: Directory where python package and dependencies are installed.
* `ooh_cache_dir`: Directory on the printer where cached lists are stored (Default: `var/cache/ooh`).
* `ooh_poll_interval`: Time in minutes between updates of the cache. Will be the maximum age of an occupancy list if the server cannot be queried when printing out (Default: 1 minute).

### Manual installation

These are the steps that Ansible carries out so can be attempted manually on
the printer controller.

* Required packages:
   * `libusb-1.0-0`
   * `python3`
* Unload and blacklist `usblp` module
* Create a virtualenv to install the software into, or decide how to maintain
  Python packages on your system.
* Install software from our repository, using pip
  `pip install --extra-index-url https://get.phy.cam.ac.uk/python/packages/print.ooh`
* Create cache directory somewhere on the system
* Install systemd service to run the monitoring process:
```unit file (systemd)
[Unit]
Description=OOH monitoring and printing
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=10
User=root
ExecStart=/path/to/your/bin/ooh.monitor
Environment="PRINT_URL=https://your.ooh.server/print"
Environment="CACHE_DIR=/path/to/cache/dir"
Environment="POLL_INTERVAL=1"

[Install]
WantedBy=multi-user.target
```