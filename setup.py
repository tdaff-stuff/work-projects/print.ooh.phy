"""
Install information.
"""

from setuptools import setup, find_packages

setup(
    name="print.ooh",
    version="1.2.4",
    description="print.ooh.phy",
    long_description="Printing utils for emergency contact lists",
    author="Tom Daff",
    author_email="tdd20@cam.ac.uk",
    license="BSD",
    url="ooh.phy.cam.ac.uk/print/",
    packages=find_packages(exclude=["tests"]),
    entry_points={
        "console_scripts": [
            "ooh.print = oohx.printing:main",
            "ooh.fetch = oohx.collect:main",
            "ooh.monitor = oohx.monitoring:main",
        ]
    },
    install_requires=["requests", "PyUSB", "RPi.GPIO"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
    ],
)
