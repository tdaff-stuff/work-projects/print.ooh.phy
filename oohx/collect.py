"""
Methods to interact with the ooh server to retrieve
a list of people for printing an emergency contact
list.

Download the register from the URL stored in the
``PRINT_URL`` environment variable.

Store a cached version in the directory given by
the ``REGISTER_DIR`` environment variable.

By default will prune anything older than one day.
Can be altered by setting MAX_AGE to the number
of seconds to keep old lists for.
"""

import argparse
import os
import time
from pathlib import Path

import requests
from requests.exceptions import RequestException


PRINT_URL = os.getenv("PRINT_URL", "https://ooh.phy.cam.ac.uk/print/")
CACHE_DIR = Path(os.getenv("CACHE_DIR", "/var/ooh/"))
MAX_AGE = float(os.getenv("MAX_AGE", 86400))


def get_page(retries=1, use_cache=True):
    """
    Get the contents of the print page.

    Return the text body of the page, or if it cannot be retrieved
    then either retry, fail or use the last successful version.
    Set max_retries to 0 to use the cached version without trying
    to get a live version.

    Parameters
    ==========
    retries: int
        Will try up to this many times to download from the server.
    use_cache: bool
        Return a cached version if a live one cannot be retrieved.
        Otherwise an IOError will be raised.
    """

    page = None

    for _retry in range(retries):
        # get the contents of the page
        try:
            page = requests.get(PRINT_URL)

            if page.status_code != 200:
                time.sleep(1)
                continue  # try again
        except RequestException:
            # network unplugged error
            continue

        cache_file = CACHE_DIR / "ooh.list.{:d}".format(int(time.time()))

        # Write the contents
        with cache_file.open("w") as outfile:
            outfile.write(page.text)

        return page.text

    else:  # no success during any attempt
        if use_cache:
            # sort by descending timestamp
            most_recent = sorted(
                CACHE_DIR.glob("ooh.list.*"),
                key=lambda x: int(x.name.split(".")[-1]),
                reverse=True,
            )[0]
            return most_recent.open().read()
        elif page is None:
            raise RuntimeError("Did nothing. Change retries or cache.")
        else:
            raise IOError(
                "Request failed with {} {}".format(
                    page.status_code, page.reason
                )
            )


def clean_cache(max_age=MAX_AGE):
    """
    Remove any files older than the requested age in seconds.

    Parameters
    ==========
    max_age: int
        Maximum age of cached file in seconds

    Returns
    =======
    removed: list of Path
        Names of files that have been removed
    """
    removed = []
    now = time.time()
    for file_path in CACHE_DIR.glob("ooh.list.*"):
        try:
            file_time = float(file_path.name.split(".")[-1])
            if now - file_time > max_age:
                file_path.unlink()
                removed.append(file_path)
        except (ValueError, KeyError, IOError):
            # don't fall over on common errors
            pass

    return removed


def main():
    """
    CLI get to get lists.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--retries",
        type=int,
        default=1,
        help="Maximum number of retry attempts",
    )
    parser.add_argument(
        "-n",
        "--no-cache",
        action="store_true",
        help="Do not use a cached list",
    )
    args = parser.parse_args()
    print(get_page(retries=args.retries, use_cache=not args.no_cache))


if __name__ == "__main__":
    main()
