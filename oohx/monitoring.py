"""
Control all the tasks that the printer needs to carry out:

- print on button press
- maintain cached copies of printouts

"""

import os
from datetime import datetime

from oohx.rpi import GPIOButton
from oohx.printing import print_text
from oohx.collect import get_page, clean_cache

# minutes -> seconds
POLL_INTERVAL = int(os.getenv("POLL_INTERVAL", 1)) * 60
# days
BANNER_INTERVAL = int(os.getenv("BANNER_INTERVAL", 5))
BANNER_TEXT = os.getenv(
    "BANNER_TEXT",
    "\n".join(
        [
            "▀\x1b!8 OUT OF HOURS \x1b!\x00▀",
            "  Press button to print a list",
            "   of people in the building.",
        ]
    ),
)


def main():
    """
    Wait for a button press to print the list, and periodically
    cache the current person list.
    """
    button = GPIOButton()
    today = -1  # not an actual day
    banner_countdown = -1  # print after starting up

    # Sometimes the printer needs a prod so try and initialise it
    # right away and fail silently while it wakes up
    try:
        print_text("")
    except OSError:
        pass

    while True:
        get_page()  # update cache
        clean_cache()  # remove old entries
        pressed = button.wait(timeout=POLL_INTERVAL * 1000)
        # returns true if button is pressed
        if pressed:
            print_text(get_page())
            # Reset banner so that it prints after ripping off
            today = -1
            banner_countdown = -1
        # Print something each day to show printer is alive
        elif datetime.now().day != today:
            today = datetime.now().day
            banner_countdown -= 1
            if banner_countdown <= 0:
                alive_text = BANNER_TEXT + "\n\n" + datetime.now().ctime()
                banner_countdown = BANNER_INTERVAL
            else:
                alive_text = datetime.now().ctime()

            print_text(alive_text)  # printer working
