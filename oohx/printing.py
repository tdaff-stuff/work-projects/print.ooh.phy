"""
Send the contents of the text to the printer.

Adapted from BasicPrint.py from ablesystems.
"""

# Probably python 3 only!
from __future__ import unicode_literals

import codecs
import platform
import sys
import time

import usb.core
import usb.util

from oohx.collect import get_page

FEED_PAST_CUTTER = b"\n" * 4
USB_BUSY = 66

PIPSTA_USB_VENDOR_ID = 0x0483
PIPSTA_USB_PRODUCT_ID = 0xA19D
AP1400_USB_PRODUCT_ID = 0xA053
AP1400V_USB_PRODUCT_ID = 0xA19C

valid_usb_ids = {
    PIPSTA_USB_PRODUCT_ID,
    AP1400_USB_PRODUCT_ID,
    AP1400V_USB_PRODUCT_ID,
}


def printer_finder(dev):
    if dev.idVendor == PIPSTA_USB_VENDOR_ID and dev.idProduct in valid_usb_ids:
        return True
    else:
        return False


def print_text(text):
    """
    Print the given text. Will convert any non-printable characters as
    question marks.
    """
    if platform.system() != "Linux":
        sys.exit("This script has only been written for Linux")

    # Find the Pipsta's specific Vendor ID and Product ID
    dev = usb.core.find(custom_match=printer_finder)
    if dev is None:  # if no such device is connected...
        raise IOError("Printer not found")  # ...report error

    try:
        # Linux requires USB devices to be reset before configuring, may not be
        # required on other operating systems.
        dev.reset()
        # Initialisation. Passing no arguments sets the configuration to the
        # currently active configuration.
        dev.set_configuration()
    except usb.core.USBError as ex:
        raise IOError("Failed to configure the printer", ex)

    # The following steps get an 'Endpoint instance'. It uses
    # PyUSB's versatile find_descriptor functionality to claim
    # the interface and get a handle to the endpoint
    # An introduction to this (forming the basis of the code below)
    # can be found at:

    cfg = (
        dev.get_active_configuration()
    )  # Get a handle to the active interface

    interface_number = cfg[(0, 0)].bInterfaceNumber
    # added to silence Linux complaint about unclaimed interface, it should be
    # release automatically
    usb.util.claim_interface(dev, interface_number)
    alternate_setting = usb.control.get_interface(dev, interface_number)
    interface = usb.util.find_descriptor(
        cfg,
        bInterfaceNumber=interface_number,
        bAlternateSetting=alternate_setting,
    )

    usb_endpoint = usb.util.find_descriptor(
        interface,
        custom_match=lambda e: usb.util.endpoint_direction(e.bEndpointAddress)
        == usb.util.ENDPOINT_OUT,
    )

    if usb_endpoint is None:  # check we have a real endpoint handle
        raise IOError("Could not find an endpoint to print to")

    # Empty text does nothing
    if not text:
        return

    # Now that the USB endpoint is open, we can start to send data to the
    # printer.
    # Printer mostly uses cp437, only difference is missing euro symbol.
    # encode replaces any non printable characters with '?'
    encoded = codecs.encode(text, "cp437", "replace")

    usb_endpoint.write(b"\x1b!\x00")

    # Print one char at a time and check the printers buffer isn't full
    for x in encoded:
        # iterating bytes gives integers, so convert to back to single bytes
        usb_endpoint.write(bytes((x,)))
        res = dev.ctrl_transfer(0xC0, 0x0E, 0x020E, 0, 2)
        while res[0] == USB_BUSY:
            time.sleep(0.01)
            res = dev.ctrl_transfer(0xC0, 0x0E, 0x020E, 0, 2)

    usb_endpoint.write(FEED_PAST_CUTTER)
    usb.util.dispose_resources(dev)


def main():
    print_text(get_page())


# Ensure that BasicPrint is ran in a stand-alone fashion (as intended) and not
# imported as a module. Prevents accidental execution of code.
if __name__ == "__main__":
    main()
