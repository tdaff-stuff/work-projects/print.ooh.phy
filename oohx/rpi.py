"""
Pi specific wait function.

"""
import time

from RPi import GPIO


class GPIOButton(object):
    def __init__(self, pin=5, led_pin=7, confirm_time=50):
        """
        GPIO button/switch.

        Parameters
        ==========
        pin: int
            Number of the GPIO pin that the button is connected
            on. Using physical BOARD numbering.
        confirm_time: int
            Time in milliseconds to wait to ensure that the button
            is pressed and not a false positive.
        """
        self.channel = pin
        self.led_channel = led_pin
        self.confirm_time = confirm_time / 1000.0
        # initialise GPIO
        GPIO.setwarnings(False)  # Ignore warning for now
        GPIO.setmode(GPIO.BOARD)  # Use physical pin numbering
        # Set pin as input pin and set initial value to be pulled high
        # connect to ground to trigger an event
        GPIO.setup(self.channel, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        if self.led_channel is not None:
            GPIO.setup(self.led_channel, GPIO.OUT)

    def wait(self, timeout=1000):
        """
        Wait for a button press on the GPIO pin.

        Parameters
        ==========
        timeout: int
            Number of milliseconds to wait for a press

        Returns
        =======
        press: bool
            True if button press has been detected, False if
            it has timed out or if a false positive has been
            detected.
        """
        # LED is on when in the waiting state
        if self.led_channel is not None:
            GPIO.output(self.led_channel, GPIO.HIGH)

        # wait for connect to ground
        event = GPIO.wait_for_edge(self.channel, GPIO.FALLING, timeout=timeout)

        # LED off if pressed or restarting loop
        if self.led_channel is not None:
            GPIO.output(self.led_channel, GPIO.LOW)

        if event is None:
            return False
        else:
            # check for false positives
            time.sleep(self.confirm_time)
            if GPIO.input(self.channel) == GPIO.LOW:
                return True
            else:
                return False

    def __del__(self):
        """Cleanup"""
        GPIO.cleanup()
