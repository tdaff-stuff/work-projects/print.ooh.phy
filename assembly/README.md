# Printer assembly instructions

## Requirements

1.  [Pipsta printer](http://www.pipsta.co.uk/) (only the printer and mounting screws are required);
2.  A Raspberry Pi (tested with RPi 2 and 3);
3.  4GB or larger microSD card;
4.  Raspberry Pi power supply (2.5 A);
5.  Network cables;
6.  Enclosure, dimensions 75 mm, 200 mm, 120 mm - [Multicomp G373](https://uk.farnell.com/multicomp/g373/box-abs-ip65-grey/dp/1175799);
7.  Illuminated pushbutton - [FL12LG5](https://uk.farnell.com/eoz/fl12lg5/pb-switch-spst-no-ip67-qc-green/dp/2770983)
    (check voltage and current if using a different LED).
8.  3 wires around 20 cm in length.
9.  68 Ohm resistor measured for **this LED**, others may require different values;
10. Solderless terminal connectors [to fit LED terminals](https://uk.farnell.com/jst-japan-solderless-terminals/sto-1-0t-110n-5/terminal-female-2-8x0-5mm/dp/3625400)
11. 3x2 crimp connector housing, [M20-1070300](https://uk.rs-online.com/web/p/pcb-connector-housings/6812859/)
    and crimp connectors.

## Steps

1.  Machine the required holes in the enclosure:
    - 74 mm x 74 mm cutout for printer, towards the top of the lid;
    - 12 mm diameter hole for pushbutton near bottom of lid;
    - 4 x 2.75 mm diameter holes for RPi mounting screws, near the bottom of the
      base, with at least 40 mm clearance on the internal right side for the
      power cable;
    - 7 mm, 5 mm and 4 mm holes at the top, in the in the gap between the two
      halves (easiest to cut in the bottom);
    - The file [printer-cutouts.svg](printer-cutouts.svg) contains a scale
      schematic that can be printed as a guide.
2.  Crimp terminal connectors to wires:
    - 2 wires with a terminal at the end;
    - 1 wire crimped with resistor into the same terminal;
    - Terminal crimped to the other end of the resistor.
3.  Crimp the other end of the wires to insert into a 3x2 connector.
4.  Connect button to GPIO connector:
    - a -> 7
    - b -> 5
    - d -> 6
    - d -> c (using resistor)
    ```
         d
        +-+        +---+
                   |5|6|
      +     +      +---+
     a|+   -|c     |7| |
      +     +      +---+
                   | | |
        +-+        +---+
         b
    ```
5.  Attach power connector to printer.
6.  Insert printer.
7.  Insert button.
8.  Prepare SD card and install it.
9.  Ensure you have the MAC address of the Pi.
10. Mount Raspberry Pi on the base.
11. Plug in network cable, power cable, USB connector.
12. Plug in and screw down ground wire.
13. Plug in button (two rows from the end of the GPIO header).
14. Insert microSD card with operating system installed.
15. Close enclosure.
16. Put Pipsta feet pads on bottom of enclosure.
17. Plug in and [finish the software installation](../README.md).

## Images

![required parts](001_parts.jpg "All required parts")
![enclosure with holes](002_enclosure.jpg "Enclosure with holes")
![button wire parts](003_button_wires.jpg "Button wire parts")
![assembled buttons](004_crimped.jpg "Assembled button wires")
![button wiring](005_button.jpg "Button wiring")
![connector wiring](006_connector.jpg "Connector wiring")
![printer parts](007_printer_parts.jpg "Printer parts")
![pi mountings](008_mounting.jpg "Pi mountings")
![mounted printer](009_printer_mount.jpg "Mounted printer")
![mounted pi](010_pi_mounted.jpg "Mounted Pi")
![fully mounted](011_fully_mounted.jpg "Fully mounted components")
![fully connected](012_fully_connected.jpg "Fully connected components")
![complete printer](013_complete.jpg "Complete printer")
